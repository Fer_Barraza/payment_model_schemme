-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ecommerce_system
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ecommerce_system` ;

-- -----------------------------------------------------
-- Schema ecommerce_system
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ecommerce_system` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `ecommerce_system` ;

-- -----------------------------------------------------
-- Table `ecommerce_system`.`Branch_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Branch_Status` (
  `Branch_Status_id` TINYINT UNSIGNED NOT NULL,
  `Branch_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Branch_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Branch`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Branch` (
  `Branch_Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Branch_Status_Branch_Status_id` TINYINT UNSIGNED NOT NULL,
  `Branch_Name` VARCHAR(45) NOT NULL,
  `Branch_Email` VARCHAR(45) NOT NULL,
  `Branch_Phone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`Branch_Id`),
  INDEX `fk_Branch_Branch_Status1_idx` (`Branch_Status_Branch_Status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Branch_Branch_Status1`
    FOREIGN KEY (`Branch_Status_Branch_Status_id`)
    REFERENCES `ecommerce_system`.`Branch_Status` (`Branch_Status_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Category` (
  `Category_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Category_Parent_id` INT UNSIGNED NULL DEFAULT NULL,
  `Category_Name` VARCHAR(35) NOT NULL,
  `Category_Url_Image` VARCHAR(120) NOT NULL DEFAULT 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png',
  PRIMARY KEY (`Category_id`),
  UNIQUE INDEX `CategoriasMenuNombre_UNIQUE` (`Category_Name` ASC) VISIBLE,
  INDEX `fk_Category_Category1_idx` (`Category_Parent_id` ASC) VISIBLE,
  CONSTRAINT `fk_Category_Category1`
    FOREIGN KEY (`Category_Parent_id`)
    REFERENCES `ecommerce_system`.`Category` (`Category_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Category_Discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Category_Discount` (
  `Category_Discount_id` INT UNSIGNED NOT NULL,
  `Category_Category_id` INT UNSIGNED NOT NULL,
  `Category_Discount_Discount_Value` FLOAT UNSIGNED NOT NULL,
  `Category_Discount_Discount_Unit` VARCHAR(20) NOT NULL,
  `Category_Discount_Date_Created` DATETIME NOT NULL,
  `Category_Discount_Valid_Until` DATETIME NOT NULL,
  `Category_Discount_Coupoun_Code` VARCHAR(10) NOT NULL,
  `Category_Discount_Minimum_Order_Value` FLOAT UNSIGNED NOT NULL DEFAULT '0',
  `Category_Discount_Maximum_Discount_Amount` FLOAT UNSIGNED NOT NULL,
  `Category_Discount_Is_Redeem_Allowed` TINYINT NOT NULL DEFAULT '0',
  PRIMARY KEY (`Category_Discount_id`, `Category_Category_id`),
  INDEX `fk_Category_Discount_Category1_idx` (`Category_Category_id` ASC) VISIBLE,
  CONSTRAINT `fk_Category_Discount_Category1`
    FOREIGN KEY (`Category_Category_id`)
    REFERENCES `ecommerce_system`.`Category` (`Category_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Countries` (
  `Countries_id` TINYINT UNSIGNED NOT NULL,
  `Countries_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Countries_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = '																																																																																																																																																							';


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Cities` (
  `Cities_id` MEDIUMINT UNSIGNED NOT NULL,
  `Countries_Countries_id` TINYINT UNSIGNED NOT NULL,
  `Cities_Name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`Cities_id`, `Countries_Countries_id`),
  INDEX `fk_Cities_Countries1_idx` (`Countries_Countries_id` ASC) VISIBLE,
  CONSTRAINT `fk_Cities_Countries1`
    FOREIGN KEY (`Countries_Countries_id`)
    REFERENCES `ecommerce_system`.`Countries` (`Countries_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = '																																																																																																																																																							';


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Customer_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Customer_Status` (
  `Customer_Status_id` TINYINT UNSIGNED NOT NULL,
  `Customer_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Customer_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Membership_Type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Membership_Type` (
  `Membership_Type_id` TINYINT UNSIGNED NOT NULL,
  `Membership_Type_Name` VARCHAR(20) NOT NULL,
  `Membership_Type_Discount_Value` FLOAT NOT NULL,
  `Membership_Type_Discount_Unit` VARCHAR(20) NOT NULL,
  `Membership_Type_Create_Date` DATETIME NOT NULL,
  `Membership_Type_Valid_Until` DATETIME NOT NULL,
  `Membership_Type_Is_Free_Shipping_Active` TINYINT NOT NULL,
  PRIMARY KEY (`Membership_Type_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Users_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Users_Status` (
  `User_Status_id` TINYINT UNSIGNED NOT NULL,
  `User_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`User_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Users` (
  `Users_id` BIGINT UNSIGNED NOT NULL,
  `Users_Status_User_Status_id` TINYINT UNSIGNED NOT NULL,
  `Users_Email` VARCHAR(45) NOT NULL,
  `Users_Name` VARCHAR(30) NOT NULL,
  `Users_Surname` VARCHAR(30) NOT NULL,
  `Users_Phone_Number` VARCHAR(20) NOT NULL,
  `Users_Profile_Picture` VARCHAR(105) NOT NULL DEFAULT 'https://www.sogapar.info/wp-content/uploads/2015/12/default-user-image.png',
  PRIMARY KEY (`Users_id`),
  INDEX `fk_Users_Users_Status1_idx` (`Users_Status_User_Status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Users_Users_Status1`
    FOREIGN KEY (`Users_Status_User_Status_id`)
    REFERENCES `ecommerce_system`.`Users_Status` (`User_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Customers` (
  `Users_id` BIGINT UNSIGNED NOT NULL,
  `Membership_Type_Membership_Type_id` TINYINT UNSIGNED NOT NULL,
  `Customer_Status_Customer_Status_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Users_id`),
  INDEX `fk_Customers_Membership_Type1_idx` (`Membership_Type_Membership_Type_id` ASC) VISIBLE,
  INDEX `fk_Customers_Customer_Status1_idx` (`Customer_Status_Customer_Status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Customers_Customer_Status1`
    FOREIGN KEY (`Customer_Status_Customer_Status_id`)
    REFERENCES `ecommerce_system`.`Customer_Status` (`Customer_Status_id`),
  CONSTRAINT `fk_Customers_Membership_Type1`
    FOREIGN KEY (`Membership_Type_Membership_Type_id`)
    REFERENCES `ecommerce_system`.`Membership_Type` (`Membership_Type_id`),
  CONSTRAINT `fk_Customers_Users1`
    FOREIGN KEY (`Users_id`)
    REFERENCES `ecommerce_system`.`Users` (`Users_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Customer_Reward_Point_Log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Customer_Reward_Point_Log` (
  `Customer_Reward_Point_Log_id` INT UNSIGNED NOT NULL,
  `Customer_Reward_Point_Log_Reward_Points` INT UNSIGNED NOT NULL DEFAULT '0',
  `Customer_Reward_Point_Log_Reward_Type` VARCHAR(2) NOT NULL,
  `Customer_Reward_Point_Log_Operation_Type` VARCHAR(1) NOT NULL,
  `Customer_Reward_Point_Log_Create_Date` DATETIME NOT NULL,
  `Customer_Reward_Point_Log_Expiry_Date` DATETIME NULL DEFAULT NULL,
  `Customers_Users_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Customer_Reward_Point_Log_id`, `Customers_Users_id`),
  INDEX `fk_Customer_Reward_Point_Log_Customers1_idx` (`Customers_Users_id` ASC) VISIBLE,
  CONSTRAINT `fk_Customer_Reward_Point_Log_Customers1`
    FOREIGN KEY (`Customers_Users_id`)
    REFERENCES `ecommerce_system`.`Customers` (`Users_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Delivery_Method`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Delivery_Method` (
  `Delivery_Method_id` SMALLINT UNSIGNED NOT NULL,
  `Delivery_Method_Name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Delivery_Method_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarter_Item_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarter_Item_Status` (
  `Headquarter_Item_Status_id` TINYINT UNSIGNED NOT NULL,
  `Headquarter_Item_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Headquarter_Item_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarter_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarter_Status` (
  `Headquarter_Status_id` TINYINT UNSIGNED NOT NULL,
  `Headquarter_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Headquarter_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Locations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Locations` (
  `Locations_id` MEDIUMINT UNSIGNED NOT NULL,
  `Cities_Cities_id` MEDIUMINT UNSIGNED NOT NULL,
  `Cities_Countries_Countries_id` TINYINT UNSIGNED NOT NULL,
  `Locations_Name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`Locations_id`, `Cities_Cities_id`, `Cities_Countries_Countries_id`),
  INDEX `fk_Locations_Cities1_idx` (`Cities_Cities_id` ASC, `Cities_Countries_Countries_id` ASC) VISIBLE,
  CONSTRAINT `fk_Locations_Cities1`
    FOREIGN KEY (`Cities_Cities_id` , `Cities_Countries_Countries_id`)
    REFERENCES `ecommerce_system`.`Cities` (`Cities_id` , `Countries_Countries_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = '																																																																																																																																																							';


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarters` (
  `Headquarters_id` MEDIUMINT UNSIGNED NOT NULL,
  `Branch_Branch_Id` MEDIUMINT UNSIGNED NOT NULL,
  `Locations_Locations_id` MEDIUMINT UNSIGNED NOT NULL,
  `Locations_Cities_Cities_id` MEDIUMINT UNSIGNED NOT NULL,
  `Locations_Cities_Countries_Countries_id` TINYINT UNSIGNED NOT NULL,
  `Headquarter_Status_Headquarter_Status_id` TINYINT UNSIGNED NOT NULL,
  `Headquarters_Address_Street_Name` VARCHAR(60) NOT NULL,
  `Headquarters_Address_Street_Number` MEDIUMINT UNSIGNED NOT NULL,
  `Headquarters_Phone` VARCHAR(20) NOT NULL,
  `Headquarters_Email` VARCHAR(45) NOT NULL,
  `Headquarters_Zip_Code` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`Headquarters_id`, `Branch_Branch_Id`),
  INDEX `fk_Headquarters_Locations1_idx` (`Locations_Locations_id` ASC, `Locations_Cities_Cities_id` ASC, `Locations_Cities_Countries_Countries_id` ASC) VISIBLE,
  INDEX `fk_Headquarters_Headquarter_Status1_idx` (`Headquarter_Status_Headquarter_Status_id` ASC) VISIBLE,
  INDEX `fk_Headquarters_Branch1_idx` (`Branch_Branch_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Headquarters_Branch1`
    FOREIGN KEY (`Branch_Branch_Id`)
    REFERENCES `ecommerce_system`.`Branch` (`Branch_Id`),
  CONSTRAINT `fk_Headquarters_Headquarter_Status1`
    FOREIGN KEY (`Headquarter_Status_Headquarter_Status_id`)
    REFERENCES `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`),
  CONSTRAINT `fk_Headquarters_Locations1`
    FOREIGN KEY (`Locations_Locations_id` , `Locations_Cities_Cities_id` , `Locations_Cities_Countries_Countries_id`)
    REFERENCES `ecommerce_system`.`Locations` (`Locations_id` , `Cities_Cities_id` , `Cities_Countries_Countries_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarters_has_Delivery_Method`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarters_has_Delivery_Method` (
  `Headquarters_Headquarters_id` MEDIUMINT UNSIGNED NOT NULL,
  `Headquarters_Branch_Branch_Id` MEDIUMINT UNSIGNED NOT NULL,
  `Delivery_Method_Delivery_Method_id` SMALLINT UNSIGNED NOT NULL,
  `Headquarters_has_Delivery_Method_Unit_Price` FLOAT UNSIGNED NOT NULL,
  PRIMARY KEY (`Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Delivery_Method_Delivery_Method_id`),
  INDEX `fk_Headquarters_has_Delivery_Method_Delivery_Method1_idx` (`Delivery_Method_Delivery_Method_id` ASC) VISIBLE,
  INDEX `fk_Headquarters_has_Delivery_Method_Headquarters1_idx` (`Headquarters_Headquarters_id` ASC, `Headquarters_Branch_Branch_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Headquarters_has_Delivery_Method_Delivery_Method1`
    FOREIGN KEY (`Delivery_Method_Delivery_Method_id`)
    REFERENCES `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`),
  CONSTRAINT `fk_Headquarters_has_Delivery_Method_Headquarters1`
    FOREIGN KEY (`Headquarters_Headquarters_id` , `Headquarters_Branch_Branch_Id`)
    REFERENCES `ecommerce_system`.`Headquarters` (`Headquarters_id` , `Branch_Branch_Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_Size`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_Size` (
  `Item_Size_id` INT UNSIGNED NOT NULL,
  `Item_Size_Name_Type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Item_Size_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_Status` (
  `Item_Status_id` TINYINT UNSIGNED NOT NULL,
  `Item_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Item_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item` (
  `Item_Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Item_CategoriasMenu_id` INT UNSIGNED NOT NULL,
  `Item_Status_Item_Status_id` TINYINT UNSIGNED NOT NULL,
  `Item_Size_Item_Size_id` INT UNSIGNED NOT NULL,
  `Item_Title` VARCHAR(45) NOT NULL,
  `Item_Currency_id` VARCHAR(5) NOT NULL DEFAULT 'ARS',
  `Item_Reference_Price` FLOAT NOT NULL,
  `Item_Description` VARCHAR(45) NOT NULL DEFAULT 'N/A',
  `Item_UpdatedAt` DATE NOT NULL,
  `Item_Rewards_Points_Credit` FLOAT UNSIGNED NOT NULL DEFAULT '0',
  `Item_UpdatedFor_User_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Item_Id`),
  INDEX `fk_Item_Category1_idx` (`Item_CategoriasMenu_id` ASC) VISIBLE,
  INDEX `fk_Item_Item_Status1_idx` (`Item_Status_Item_Status_id` ASC) VISIBLE,
  INDEX `fk_Item_Item_Size1_idx` (`Item_Size_Item_Size_id` ASC) VISIBLE,
  CONSTRAINT `fk_Item_Category1`
    FOREIGN KEY (`Item_CategoriasMenu_id`)
    REFERENCES `ecommerce_system`.`Category` (`Category_id`),
  CONSTRAINT `fk_Item_Item_Size1`
    FOREIGN KEY (`Item_Size_Item_Size_id`)
    REFERENCES `ecommerce_system`.`Item_Size` (`Item_Size_id`),
  CONSTRAINT `fk_Item_Item_Status1`
    FOREIGN KEY (`Item_Status_Item_Status_id`)
    REFERENCES `ecommerce_system`.`Item_Status` (`Item_Status_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarters_has_Item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarters_has_Item` (
  `Item_Item_Id` BIGINT UNSIGNED NOT NULL,
  `Headquarters_Headquarters_id` MEDIUMINT UNSIGNED NOT NULL,
  `Headquarters_Branch_Branch_Id` MEDIUMINT UNSIGNED NOT NULL,
  `Headquarter_Item_Status_id` TINYINT UNSIGNED NOT NULL,
  `Headquarters_has_Item_Unit_Price` FLOAT UNSIGNED NOT NULL,
  `Headquarters_has_Item_Price_UpdatedAt` DATE NOT NULL,
  PRIMARY KEY (`Item_Item_Id`, `Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`),
  INDEX `fk_Headquarters_has_Item_Item1_idx` (`Item_Item_Id` ASC) VISIBLE,
  INDEX `fk_Headquarters_has_Item_Headquarter_Item_Status1_idx` (`Headquarter_Item_Status_id` ASC) VISIBLE,
  INDEX `fk_Headquarters_has_Item_Headquarters1_idx` (`Headquarters_Headquarters_id` ASC, `Headquarters_Branch_Branch_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Headquarters_has_Item_Headquarter_Item_Status1`
    FOREIGN KEY (`Headquarter_Item_Status_id`)
    REFERENCES `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`),
  CONSTRAINT `fk_Headquarters_has_Item_Headquarters1`
    FOREIGN KEY (`Headquarters_Headquarters_id` , `Headquarters_Branch_Branch_Id`)
    REFERENCES `ecommerce_system`.`Headquarters` (`Headquarters_id` , `Branch_Branch_Id`),
  CONSTRAINT `fk_Headquarters_has_Item_Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Sellers_Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Sellers_Category` (
  `Sellers_Category_id` TINYINT UNSIGNED NOT NULL,
  `Sellers_Category_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Sellers_Category_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Sellers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Sellers` (
  `Users_id` BIGINT UNSIGNED NOT NULL,
  `Sellers_Category_Sellers_Category_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Users_id`),
  INDEX `fk_Sellers_Sellers_Category1_idx` (`Sellers_Category_Sellers_Category_id` ASC) VISIBLE,
  CONSTRAINT `fk_Sellers_Sellers_Category1`
    FOREIGN KEY (`Sellers_Category_Sellers_Category_id`)
    REFERENCES `ecommerce_system`.`Sellers_Category` (`Sellers_Category_id`),
  CONSTRAINT `fk_Sellers_Users1`
    FOREIGN KEY (`Users_id`)
    REFERENCES `ecommerce_system`.`Users` (`Users_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Headquarters_has_Sellers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Headquarters_has_Sellers` (
  `Headquarters_Headquarters_id` MEDIUMINT UNSIGNED NOT NULL,
  `Headquarters_Branch_Branch_Id` MEDIUMINT UNSIGNED NOT NULL,
  `Sellers_Users_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Sellers_Users_id`),
  INDEX `fk_Headquarters_has_Sellers_Sellers1_idx` (`Sellers_Users_id` ASC) VISIBLE,
  INDEX `fk_Headquarters_has_Sellers_Headquarters1_idx` (`Headquarters_Headquarters_id` ASC, `Headquarters_Branch_Branch_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Headquarters_has_Sellers_Headquarters1`
    FOREIGN KEY (`Headquarters_Headquarters_id` , `Headquarters_Branch_Branch_Id`)
    REFERENCES `ecommerce_system`.`Headquarters` (`Headquarters_id` , `Branch_Branch_Id`),
  CONSTRAINT `fk_Headquarters_has_Sellers_Sellers1`
    FOREIGN KEY (`Sellers_Users_id`)
    REFERENCES `ecommerce_system`.`Sellers` (`Users_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_Discount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_Discount` (
  `Item_Discount_id` INT UNSIGNED NOT NULL,
  `Item_Item_Id` BIGINT UNSIGNED NOT NULL,
  `Item_Discount_Discount_Value` FLOAT UNSIGNED NOT NULL,
  `Item_Discount_Discount_Unit` VARCHAR(20) NOT NULL,
  `Item_Discount_Date_Created` DATETIME NOT NULL,
  `Item_Discount_Valid_Until` DATETIME NOT NULL,
  `Item_Discount_Coupoun_Code` VARCHAR(10) NOT NULL,
  `Item_Discount_Minimum_Order_Value` FLOAT UNSIGNED NOT NULL DEFAULT '0',
  `Item_Discount_Maximum_Discount_Amount` FLOAT UNSIGNED NOT NULL,
  `Item_Discount_Is_Redeem_Allowed` TINYINT NOT NULL DEFAULT '0',
  PRIMARY KEY (`Item_Discount_id`, `Item_Item_Id`),
  INDEX `fk_Category_Discount_copy1_Item1_idx` (`Item_Item_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Category_Discount_copy1_Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_Size_Color_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_Size_Color_Status` (
  `Item_Size_Color_Status_id` TINYINT UNSIGNED NOT NULL,
  `Item_Size_Color_Status_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Item_Size_Color_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Ref_Colours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Ref_Colours` (
  `Color_Code` TINYINT UNSIGNED NOT NULL,
  `Color_Description` VARCHAR(22) NOT NULL,
  PRIMARY KEY (`Color_Code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Ref_Sizes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Ref_Sizes` (
  `Size_Code` TINYINT NOT NULL,
  `Size_Description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Size_Code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_With_Size_Color`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_With_Size_Color` (
  `Item_Item_Id` BIGINT UNSIGNED NOT NULL,
  `Ref_Colours_Color_Code` TINYINT UNSIGNED NOT NULL,
  `Ref_Sizes_Size_Code` TINYINT NOT NULL,
  `Item_Size_Color_Status_Item_Size_Color_Status_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Item_Item_Id`),
  INDEX `fk_Item_With__Ref_Colours1_idx` (`Ref_Colours_Color_Code` ASC) VISIBLE,
  INDEX `fk_Item_With__Ref_Sizes1_idx` (`Ref_Sizes_Size_Code` ASC) VISIBLE,
  INDEX `fk_Item_With_Size_Color_Item_Size_Color_Status1_idx` (`Item_Size_Color_Status_Item_Size_Color_Status_id` ASC) VISIBLE,
  CONSTRAINT `fk_Item_With__Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`),
  CONSTRAINT `fk_Item_With__Ref_Colours1`
    FOREIGN KEY (`Ref_Colours_Color_Code`)
    REFERENCES `ecommerce_system`.`Ref_Colours` (`Color_Code`),
  CONSTRAINT `fk_Item_With__Ref_Sizes1`
    FOREIGN KEY (`Ref_Sizes_Size_Code`)
    REFERENCES `ecommerce_system`.`Ref_Sizes` (`Size_Code`),
  CONSTRAINT `fk_Item_With_Size_Color_Item_Size_Color_Status1`
    FOREIGN KEY (`Item_Size_Color_Status_Item_Size_Color_Status_id`)
    REFERENCES `ecommerce_system`.`Item_Size_Color_Status` (`Item_Size_Color_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Order_Shipping_Method`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Order_Shipping_Method` (
  `Order_Shipping_Method_id` TINYINT UNSIGNED NOT NULL,
  `Order_Shipping_Method_Name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Order_Shipping_Method_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Order_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Order_Status` (
  `Order_Status_Id` TINYINT NOT NULL AUTO_INCREMENT,
  `Order_Status_Type` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`Order_Status_Id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Payment_Methods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Payment_Methods` (
  `Payment_Methods_id` TINYINT UNSIGNED NOT NULL,
  `Payment_Methods_Type_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Payment_Methods_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Orders` (
  `Order_Id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Order_Shipping_Method_Order_Shipping_Method_id` TINYINT UNSIGNED NOT NULL,
  `Order_Status_Order_Status_Id` TINYINT NOT NULL,
  `Customers_Users_id` BIGINT UNSIGNED NOT NULL,
  `Payment_Methods_Payment_Methods_id` TINYINT UNSIGNED NOT NULL,
  `Orders_UpdatedFor_User_id` BIGINT UNSIGNED NOT NULL,
  `Order_Payment_Total_Price` FLOAT UNSIGNED NOT NULL,
  `Order_Date_Placed` DATE NOT NULL,
  `Order_Time_Placed` TIME NOT NULL,
  `Orders_Date_Updated` DATE NOT NULL,
  `Orders_Time_Updated` TIME NOT NULL,
  PRIMARY KEY (`Order_Id`),
  INDEX `fk_Orders_Order_Shipping_Method1_idx` (`Order_Shipping_Method_Order_Shipping_Method_id` ASC) VISIBLE,
  INDEX `fk_Orders_Order_Status1_idx` (`Order_Status_Order_Status_Id` ASC) VISIBLE,
  INDEX `fk_Orders_Payment_Methods1_idx` (`Payment_Methods_Payment_Methods_id` ASC) VISIBLE,
  INDEX `fk_Orders_Customers1_idx` (`Customers_Users_id` ASC) VISIBLE,
  INDEX `fk_Orders_Users1_idx` (`Orders_UpdatedFor_User_id` ASC) VISIBLE,
  CONSTRAINT `fk_Orders_Customers1`
    FOREIGN KEY (`Customers_Users_id`)
    REFERENCES `ecommerce_system`.`Customers` (`Users_id`),
  CONSTRAINT `fk_Orders_Order_Shipping_Method1`
    FOREIGN KEY (`Order_Shipping_Method_Order_Shipping_Method_id`)
    REFERENCES `ecommerce_system`.`Order_Shipping_Method` (`Order_Shipping_Method_id`),
  CONSTRAINT `fk_Orders_Order_Status1`
    FOREIGN KEY (`Order_Status_Order_Status_Id`)
    REFERENCES `ecommerce_system`.`Order_Status` (`Order_Status_Id`),
  CONSTRAINT `fk_Orders_Payment_Methods1`
    FOREIGN KEY (`Payment_Methods_Payment_Methods_id`)
    REFERENCES `ecommerce_system`.`Payment_Methods` (`Payment_Methods_id`),
  CONSTRAINT `fk_Orders_Users1`
    FOREIGN KEY (`Orders_UpdatedFor_User_id`)
    REFERENCES `ecommerce_system`.`Users` (`Users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Item_has_Order_Detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Item_has_Order_Detail` (
  `Order_Order_Id` BIGINT UNSIGNED NOT NULL,
  `Item_Item_Id` BIGINT UNSIGNED NOT NULL,
  `Item_has_Order_Detail_Quantity` MEDIUMINT UNSIGNED NOT NULL,
  `Item_has_Order_Detail_Unit_Price` FLOAT NOT NULL,
  PRIMARY KEY (`Order_Order_Id`, `Item_Item_Id`),
  INDEX `fk_Item_has_Order_Item1_idx` (`Item_Item_Id` ASC) VISIBLE,
  INDEX `fk_Item_has_Order_Order1_idx` (`Order_Order_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Item_has_Order_Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Item_has_Order_Order1`
    FOREIGN KEY (`Order_Order_Id`)
    REFERENCES `ecommerce_system`.`Orders` (`Order_Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`MP_Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`MP_Orders` (
  `Orders_Order_Id` BIGINT UNSIGNED NOT NULL,
  `MP_Orders_Code` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Orders_Order_Id`),
  CONSTRAINT `fk_MP_Orders_Orders1`
    FOREIGN KEY (`Orders_Order_Id`)
    REFERENCES `ecommerce_system`.`Orders` (`Order_Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Shipment_Status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Shipment_Status` (
  `Shipment_Status_id` SMALLINT UNSIGNED NOT NULL,
  `Shipment_Status_Description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Shipment_Status_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Order_Shipments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Order_Shipments` (
  `Orders_Order_Id` BIGINT UNSIGNED NOT NULL,
  `Order_Shipments_id` BIGINT UNSIGNED NOT NULL,
  `Delivery_Method_Delivery_Method_id` SMALLINT UNSIGNED NOT NULL,
  `Shipment_Status_Shipment_Status_id` SMALLINT UNSIGNED NOT NULL,
  `Order_Shipments_Delivery_Address_To_Street_Name` VARCHAR(60) NOT NULL,
  `Order_Shipments_Delivery_Address_To_Street_Number` MEDIUMINT UNSIGNED NOT NULL,
  `Order_Shipments_Date_Placed` DATE NOT NULL,
  `Order_Shipments_Time_Placed` TIME NOT NULL,
  `Headquarters_id` MEDIUMINT UNSIGNED NOT NULL,
  `Branch_Id` MEDIUMINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Orders_Order_Id`, `Order_Shipments_id`),
  INDEX `fk_Shipments_Delivery_Method1_idx` (`Delivery_Method_Delivery_Method_id` ASC) VISIBLE,
  INDEX `fk_Shipments_Shipment_Status1_idx` (`Shipment_Status_Shipment_Status_id` ASC) VISIBLE,
  INDEX `fk_Order_Shipments_Orders1_idx` (`Orders_Order_Id` ASC) VISIBLE,
  INDEX `fk_Order_Shipments_Headquarters1_idx` (`Headquarters_id` ASC, `Branch_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Order_Shipments_Orders1`
    FOREIGN KEY (`Orders_Order_Id`)
    REFERENCES `ecommerce_system`.`Orders` (`Order_Id`),
  CONSTRAINT `fk_Shipments_Delivery_Method1`
    FOREIGN KEY (`Delivery_Method_Delivery_Method_id`)
    REFERENCES `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`),
  CONSTRAINT `fk_Shipments_Shipment_Status1`
    FOREIGN KEY (`Shipment_Status_Shipment_Status_id`)
    REFERENCES `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`),
  CONSTRAINT `fk_Order_Shipments_Headquarters1`
    FOREIGN KEY (`Headquarters_id` , `Branch_Id`)
    REFERENCES `ecommerce_system`.`Headquarters` (`Headquarters_id` , `Branch_Branch_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Payment_Offer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Payment_Offer` (
  `Payment_Offer_id` INT UNSIGNED NOT NULL,
  `Category_Category_id` INT UNSIGNED NULL DEFAULT NULL,
  `Item_Item_Id` BIGINT UNSIGNED NULL DEFAULT NULL,
  `Payment_Offer_Institute_Type` VARCHAR(50) NOT NULL,
  `Payment_Offer_Institute_Name` VARCHAR(200) NOT NULL,
  `Payment_Offer_Card_Type` VARCHAR(20) NULL DEFAULT NULL,
  `Payment_Offer_Coupon_Code` VARCHAR(10) NOT NULL,
  `Payment_Offer_Discount_Value` FLOAT UNSIGNED NOT NULL,
  `Payment_Offer_Discount_Unit` VARCHAR(20) NOT NULL,
  `Payment_Offer_Date_Created` DATETIME NOT NULL,
  `Payment_Offer_Valid_Until` DATETIME NOT NULL,
  `Payment_Offer_Maximum_Discount_Amount` FLOAT UNSIGNED NOT NULL,
  PRIMARY KEY (`Payment_Offer_id`),
  INDEX `fk_Payment_Offer_Category1_idx` (`Category_Category_id` ASC) VISIBLE,
  INDEX `fk_Payment_Offer_Item1_idx` (`Item_Item_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Payment_Offer_Category1`
    FOREIGN KEY (`Category_Category_id`)
    REFERENCES `ecommerce_system`.`Category` (`Category_id`),
  CONSTRAINT `fk_Payment_Offer_Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Url_images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Url_images` (
  `Url_images` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Item_Item_Id` BIGINT UNSIGNED NOT NULL,
  `Url_images_Location` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`Url_images`, `Item_Item_Id`),
  INDEX `fk_Url_images_Item1_idx` (`Item_Item_Id` ASC) VISIBLE,
  CONSTRAINT `fk_Url_images_Item1`
    FOREIGN KEY (`Item_Item_Id`)
    REFERENCES `ecommerce_system`.`Item` (`Item_Id`))
ENGINE = InnoDB
AUTO_INCREMENT = 101
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `ecommerce_system`.`Orders_Reshipment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ecommerce_system`.`Orders_Reshipment` (
  `Orders_Reshipment_Id` INT UNSIGNED NOT NULL,
  `Order_Shipments_Orders_Order_Id` BIGINT UNSIGNED NOT NULL,
  `Order_Shipments_Order_Shipments_id` BIGINT UNSIGNED NOT NULL,
  `Orders_Reshipment_Street_Name_To` VARCHAR(60) NOT NULL,
  `Orders_Reshipment_Street_Number_To` MEDIUMINT UNSIGNED NOT NULL,
  PRIMARY KEY (`Orders_Reshipment_Id`, `Order_Shipments_Orders_Order_Id`, `Order_Shipments_Order_Shipments_id`),
  INDEX `fk_Orders_Reshipment_Order_Shipments1_idx` (`Order_Shipments_Orders_Order_Id` ASC, `Order_Shipments_Order_Shipments_id` ASC) VISIBLE,
  CONSTRAINT `fk_Orders_Reshipment_Order_Shipments1`
    FOREIGN KEY (`Order_Shipments_Orders_Order_Id` , `Order_Shipments_Order_Shipments_id`)
    REFERENCES `ecommerce_system`.`Order_Shipments` (`Orders_Order_Id` , `Order_Shipments_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Branch_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (1, 'Active');
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (2, 'Deactive');
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (3, 'Instantiated');
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (4, 'Deleted');
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (5, 'Suspended');
INSERT INTO `ecommerce_system`.`Branch_Status` (`Branch_Status_id`, `Branch_Status_Type_Name`) VALUES (6, 'Not working');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Branch`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Branch` (`Branch_Id`, `Branch_Status_Branch_Status_id`, `Branch_Name`, `Branch_Email`, `Branch_Phone`) VALUES (1, 1, 'Fabrikapps', 'fabrikapps@gmail.com', '113333333');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Category`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Category` (`Category_id`, `Category_Parent_id`, `Category_Name`, `Category_Url_Image`) VALUES (1, NULL, 'Producto', DEFAULT);
INSERT INTO `ecommerce_system`.`Category` (`Category_id`, `Category_Parent_id`, `Category_Name`, `Category_Url_Image`) VALUES (2, 1, 'Comida', DEFAULT);
INSERT INTO `ecommerce_system`.`Category` (`Category_id`, `Category_Parent_id`, `Category_Name`, `Category_Url_Image`) VALUES (3, 2, 'Comida rapida', 'https://media-cdn.tripadvisor.com/media/photo-s/17/ad/2f/16/double-burger.jpg');
INSERT INTO `ecommerce_system`.`Category` (`Category_id`, `Category_Parent_id`, `Category_Name`, `Category_Url_Image`) VALUES (4, 3, 'Hamburguesas', 'https://thecookandthewine.files.wordpress.com/2020/05/big-burger-1_opt.jpg');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Countries`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Countries` (`Countries_id`, `Countries_Name`) VALUES (1, 'Argentina');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Cities`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Cities` (`Cities_id`, `Countries_Countries_id`, `Cities_Name`) VALUES (1, 1, 'Buenos Aires');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Customer_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Customer_Status` (`Customer_Status_id`, `Customer_Status_Type_Name`) VALUES (1, 'Unregistered');
INSERT INTO `ecommerce_system`.`Customer_Status` (`Customer_Status_id`, `Customer_Status_Type_Name`) VALUES (2, 'Registered, without orders');
INSERT INTO `ecommerce_system`.`Customer_Status` (`Customer_Status_id`, `Customer_Status_Type_Name`) VALUES (3, 'Registered, with orders');
INSERT INTO `ecommerce_system`.`Customer_Status` (`Customer_Status_id`, `Customer_Status_Type_Name`) VALUES (4, 'Suspended');
INSERT INTO `ecommerce_system`.`Customer_Status` (`Customer_Status_id`, `Customer_Status_Type_Name`) VALUES (5, 'Deleted');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Membership_Type`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Membership_Type` (`Membership_Type_id`, `Membership_Type_Name`, `Membership_Type_Discount_Value`, `Membership_Type_Discount_Unit`, `Membership_Type_Create_Date`, `Membership_Type_Valid_Until`, `Membership_Type_Is_Free_Shipping_Active`) VALUES (1, 'Normal', 0, 'ARS', '2021-07-14', '2022-07-14', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Users_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (1, 'Unregistered');
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (2, 'Suspended');
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (3, 'Registered');
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (4, 'Deleted');
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (5, 'Activated');
INSERT INTO `ecommerce_system`.`Users_Status` (`User_Status_id`, `User_Status_Type_Name`) VALUES (6, 'Deactivated');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Users`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Users` (`Users_id`, `Users_Status_User_Status_id`, `Users_Email`, `Users_Name`, `Users_Surname`, `Users_Phone_Number`, `Users_Profile_Picture`) VALUES (1, 5, 'fer@gmail.com', 'Fer', 'Bar', '116656565', 'https://lh3.googleusercontent.com/ogw/ADea4I55Y63mdAZdfYGABTxeDGiKlhByE-VqGEG6DF8RRg');
INSERT INTO `ecommerce_system`.`Users` (`Users_id`, `Users_Status_User_Status_id`, `Users_Email`, `Users_Name`, `Users_Surname`, `Users_Phone_Number`, `Users_Profile_Picture`) VALUES (2, 5, 'car@gmail.com', 'Car', 'Nig', '114454546', DEFAULT);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Customers`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Customers` (`Users_id`, `Membership_Type_Membership_Type_id`, `Customer_Status_Customer_Status_id`) VALUES (2, 1, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Delivery_Method`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (1, 'Take away');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (2, 'Transport delivery');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (3, 'Immediate delivery');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (4, 'Moto delivery');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (5, 'Bike delivery');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (6, 'Car delivery');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (7, 'Delivery truck');
INSERT INTO `ecommerce_system`.`Delivery_Method` (`Delivery_Method_id`, `Delivery_Method_Name`) VALUES (8, 'Delivery walk');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarter_Item_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (1, 'Available');
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (2, 'Not available');
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (3, 'Deleted');
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (4, 'Out of stock');
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (5, 'Instantiated');
INSERT INTO `ecommerce_system`.`Headquarter_Item_Status` (`Headquarter_Item_Status_id`, `Headquarter_Item_Status_Type_Name`) VALUES (6, 'Few left in stock');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarter_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (1, 'Active');
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (2, 'Deactive');
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (3, 'Instatiated');
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (4, 'Deleted');
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (5, 'Suspended');
INSERT INTO `ecommerce_system`.`Headquarter_Status` (`Headquarter_Status_id`, `Headquarter_Status_Type_Name`) VALUES (6, 'Not working');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Locations`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Locations` (`Locations_id`, `Cities_Cities_id`, `Cities_Countries_Countries_id`, `Locations_Name`) VALUES (1, 1, 1, 'Monte Grande');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarters`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarters` (`Headquarters_id`, `Branch_Branch_Id`, `Locations_Locations_id`, `Locations_Cities_Cities_id`, `Locations_Cities_Countries_Countries_id`, `Headquarter_Status_Headquarter_Status_id`, `Headquarters_Address_Street_Name`, `Headquarters_Address_Street_Number`, `Headquarters_Phone`, `Headquarters_Email`, `Headquarters_Zip_Code`) VALUES (1, 1, 1, 1, 1, 1, 'Rivadavia', 663, '113828888', 'fabrikapps-mg@gmail.com', '1842');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarters_has_Delivery_Method`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarters_has_Delivery_Method` (`Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Delivery_Method_Delivery_Method_id`, `Headquarters_has_Delivery_Method_Unit_Price`) VALUES (1, 1, 2, 50);
INSERT INTO `ecommerce_system`.`Headquarters_has_Delivery_Method` (`Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Delivery_Method_Delivery_Method_id`, `Headquarters_has_Delivery_Method_Unit_Price`) VALUES (1, 1, 1, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Item_Size`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (1, 'Super small');
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (2, 'Small');
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (3, 'Medium');
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (4, 'Large');
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (5, 'Super large');
INSERT INTO `ecommerce_system`.`Item_Size` (`Item_Size_id`, `Item_Size_Name_Type`) VALUES (6, 'Giant');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Item_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Item_Status` (`Item_Status_id`, `Item_Status_Type_Name`) VALUES (1, 'Available');
INSERT INTO `ecommerce_system`.`Item_Status` (`Item_Status_id`, `Item_Status_Type_Name`) VALUES (2, 'Not available');
INSERT INTO `ecommerce_system`.`Item_Status` (`Item_Status_id`, `Item_Status_Type_Name`) VALUES (3, 'Deleted');
INSERT INTO `ecommerce_system`.`Item_Status` (`Item_Status_id`, `Item_Status_Type_Name`) VALUES (4, 'Out of stock');
INSERT INTO `ecommerce_system`.`Item_Status` (`Item_Status_id`, `Item_Status_Type_Name`) VALUES (5, 'Instantiated');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Item`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Item` (`Item_Id`, `Item_CategoriasMenu_id`, `Item_Status_Item_Status_id`, `Item_Size_Item_Size_id`, `Item_Title`, `Item_Currency_id`, `Item_Reference_Price`, `Item_Description`, `Item_UpdatedAt`, `Item_Rewards_Points_Credit`, `Item_UpdatedFor_User_id`) VALUES (1, 4, 1, 2, 'Matadora', 'ARS', 350, 'Lechuga y tomate', '2021-07-14', 0, 1);
INSERT INTO `ecommerce_system`.`Item` (`Item_Id`, `Item_CategoriasMenu_id`, `Item_Status_Item_Status_id`, `Item_Size_Item_Size_id`, `Item_Title`, `Item_Currency_id`, `Item_Reference_Price`, `Item_Description`, `Item_UpdatedAt`, `Item_Rewards_Points_Credit`, `Item_UpdatedFor_User_id`) VALUES (2, 4, 1, 2, 'Campeona', 'ARS', 400, 'Queso cheddar', '2021-07-14', 0, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarters_has_Item`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarters_has_Item` (`Item_Item_Id`, `Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Headquarter_Item_Status_id`, `Headquarters_has_Item_Unit_Price`, `Headquarters_has_Item_Price_UpdatedAt`) VALUES (1, 1, 1, 1, 350, '2021-07-14');
INSERT INTO `ecommerce_system`.`Headquarters_has_Item` (`Item_Item_Id`, `Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Headquarter_Item_Status_id`, `Headquarters_has_Item_Unit_Price`, `Headquarters_has_Item_Price_UpdatedAt`) VALUES (2, 1, 1, 1, 450, '2021-07-14');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Sellers_Category`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Sellers_Category` (`Sellers_Category_id`, `Sellers_Category_Type_Name`) VALUES (1, 'Administrator');
INSERT INTO `ecommerce_system`.`Sellers_Category` (`Sellers_Category_id`, `Sellers_Category_Type_Name`) VALUES (2, 'Worker');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Sellers`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Sellers` (`Users_id`, `Sellers_Category_Sellers_Category_id`) VALUES (1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Headquarters_has_Sellers`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Headquarters_has_Sellers` (`Headquarters_Headquarters_id`, `Headquarters_Branch_Branch_Id`, `Sellers_Users_id`) VALUES (1, 1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Order_Shipping_Method`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Order_Shipping_Method` (`Order_Shipping_Method_id`, `Order_Shipping_Method_Name`) VALUES (1, 'Take away');
INSERT INTO `ecommerce_system`.`Order_Shipping_Method` (`Order_Shipping_Method_id`, `Order_Shipping_Method_Name`) VALUES (2, 'Transport delivery');
INSERT INTO `ecommerce_system`.`Order_Shipping_Method` (`Order_Shipping_Method_id`, `Order_Shipping_Method_Name`) VALUES (3, 'Immediate delivery');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Order_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (1, 'Waiting for');
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (2, 'Canceled');
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (3, 'In process');
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (4, 'Delivered');
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (5, 'In possession of the buyer');
INSERT INTO `ecommerce_system`.`Order_Status` (`Order_Status_Id`, `Order_Status_Type`) VALUES (6, 'Finished');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Payment_Methods`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Payment_Methods` (`Payment_Methods_id`, `Payment_Methods_Type_Name`) VALUES (1, 'Credit card');
INSERT INTO `ecommerce_system`.`Payment_Methods` (`Payment_Methods_id`, `Payment_Methods_Type_Name`) VALUES (2, 'Debit card');
INSERT INTO `ecommerce_system`.`Payment_Methods` (`Payment_Methods_id`, `Payment_Methods_Type_Name`) VALUES (3, 'Cash payment');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Orders`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Orders` (`Order_Id`, `Order_Shipping_Method_Order_Shipping_Method_id`, `Order_Status_Order_Status_Id`, `Customers_Users_id`, `Payment_Methods_Payment_Methods_id`, `Orders_UpdatedFor_User_id`, `Order_Payment_Total_Price`, `Order_Date_Placed`, `Order_Time_Placed`, `Orders_Date_Updated`, `Orders_Time_Updated`) VALUES (1, 2, 4, 2, 1, 1, 700, '2021-07-17', '04:30', '2021-07-17', '04:40');
INSERT INTO `ecommerce_system`.`Orders` (`Order_Id`, `Order_Shipping_Method_Order_Shipping_Method_id`, `Order_Status_Order_Status_Id`, `Customers_Users_id`, `Payment_Methods_Payment_Methods_id`, `Orders_UpdatedFor_User_id`, `Order_Payment_Total_Price`, `Order_Date_Placed`, `Order_Time_Placed`, `Orders_Date_Updated`, `Orders_Time_Updated`) VALUES (2, 2, 4, 2, 2, 1, 1600, '2021-07-17', '04:50', '2021-07-17', '05:40');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Item_has_Order_Detail`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Item_has_Order_Detail` (`Order_Order_Id`, `Item_Item_Id`, `Item_has_Order_Detail_Quantity`, `Item_has_Order_Detail_Unit_Price`) VALUES (1, 1, 2, 350);
INSERT INTO `ecommerce_system`.`Item_has_Order_Detail` (`Order_Order_Id`, `Item_Item_Id`, `Item_has_Order_Detail_Quantity`, `Item_has_Order_Detail_Unit_Price`) VALUES (2, 1, 2, 350);
INSERT INTO `ecommerce_system`.`Item_has_Order_Detail` (`Order_Order_Id`, `Item_Item_Id`, `Item_has_Order_Detail_Quantity`, `Item_has_Order_Detail_Unit_Price`) VALUES (2, 2, 2, 450);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`MP_Orders`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`MP_Orders` (`Orders_Order_Id`, `MP_Orders_Code`) VALUES (1, 99999999999);
INSERT INTO `ecommerce_system`.`MP_Orders` (`Orders_Order_Id`, `MP_Orders_Code`) VALUES (2, 999999);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Shipment_Status`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (1, 'Delivered');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (2, 'In process');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (3, 'En route to the destination');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (4, 'Canceled');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (5, 'In possession of the buyer');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (6, 'Delivery failed');
INSERT INTO `ecommerce_system`.`Shipment_Status` (`Shipment_Status_id`, `Shipment_Status_Description`) VALUES (7, 'Interrupted on the way');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Order_Shipments`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Order_Shipments` (`Orders_Order_Id`, `Order_Shipments_id`, `Delivery_Method_Delivery_Method_id`, `Shipment_Status_Shipment_Status_id`, `Order_Shipments_Delivery_Address_To_Street_Name`, `Order_Shipments_Delivery_Address_To_Street_Number`, `Order_Shipments_Date_Placed`, `Order_Shipments_Time_Placed`, `Headquarters_id`, `Branch_Id`) VALUES (1, 1, 1, 1, 'Cerca', 999, '2021-07-17', '04:40', 1, 1);
INSERT INTO `ecommerce_system`.`Order_Shipments` (`Orders_Order_Id`, `Order_Shipments_id`, `Delivery_Method_Delivery_Method_id`, `Shipment_Status_Shipment_Status_id`, `Order_Shipments_Delivery_Address_To_Street_Name`, `Order_Shipments_Delivery_Address_To_Street_Number`, `Order_Shipments_Date_Placed`, `Order_Shipments_Time_Placed`, `Headquarters_id`, `Branch_Id`) VALUES (2, 1, 2, 1, 'Cerca', 999, '2021-07-17', '05:40', 1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ecommerce_system`.`Url_images`
-- -----------------------------------------------------
START TRANSACTION;
USE `ecommerce_system`;
INSERT INTO `ecommerce_system`.`Url_images` (`Url_images`, `Item_Item_Id`, `Url_images_Location`) VALUES (1, 1, 'https://static.wixstatic.com/media/730513_bde24b396362445a8c31b2bc9506b0f8~mv2.jpg/v1/fill/w_800,h_800,al_c,q_90/730513_bde24b396362445a8c31b2bc9506b0f8~mv2.jpg');
INSERT INTO `ecommerce_system`.`Url_images` (`Url_images`, `Item_Item_Id`, `Url_images_Location`) VALUES (2, 1, 'https://lh3.googleusercontent.com/proxy/wWrSeAjp-0z2h3PYSzcg0M7bOFTAlXoLvrHxbGTBJNTcEke4XQreBq3mys_vroaUks5jqtQbfjDDZDh_M-vmq8_d8gUvvCzTI5o');

COMMIT;

