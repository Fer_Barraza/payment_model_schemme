####### Customer
# 									User stories for orders

# Create order with take away, cash payment
INSERT INTO ecommerce_system.Orders
(Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated)
VALUES(1, 3, 2, 3, 2, 450.99, CURRENT_DATE(), CURRENT_TIME(), CURRENT_DATE(), CURRENT_TIME());

# Create order with transport delivery, cash payment
INSERT INTO ecommerce_system.Orders
(Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated)
VALUES(2, 3, 2, 3, 2, 450.99, CURRENT_DATE(), CURRENT_TIME(), CURRENT_DATE(), CURRENT_TIME());

# Create order with take away, debit payment
INSERT INTO ecommerce_system.Orders
(Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated)
VALUES(1, 3, 2, 2, 2, 450.99, CURRENT_DATE(), CURRENT_TIME(), CURRENT_DATE(), CURRENT_TIME());

# Create order with transport delivery, debit payment
INSERT INTO ecommerce_system.Orders
(Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated)
VALUES(2, 3, 2, 2, 2, 450.99, CURRENT_DATE(), CURRENT_TIME(), CURRENT_DATE(), CURRENT_TIME());

# Get track of order by id
SELECT Order_Id, Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated
FROM ecommerce_system.Orders WHERE Order_Id = 1;

# Get all orders from las 3 days
SELECT Order_Id, Order_Shipping_Method_Order_Shipping_Method_id, Order_Status_Order_Status_Id, Customers_Users_id, Payment_Methods_Payment_Methods_id, Orders_UpdatedFor_User_id, Order_Payment_Total_Price, Order_Date_Placed, Order_Time_Placed, Orders_Date_Updated, Orders_Time_Updated
FROM ecommerce_system.Orders WHERE Order_Date_Placed >= DATE_ADD(CURDATE(), INTERVAL -3 DAY);

# Cancel order
UPDATE ecommerce_system.Orders
SET Order_Status_Order_Status_Id=2, Orders_UpdatedFor_User_id=1, Orders_Date_Updated = CURRENT_DATE(), Orders_Time_Updated = CURRENT_TIME() 
WHERE Order_Id = 1;

# Update status order
UPDATE ecommerce_system.Orders
SET Order_Status_Order_Status_Id=4, Orders_UpdatedFor_User_id=1, Orders_Date_Updated = CURRENT_DATE(), Orders_Time_Updated = CURRENT_TIME() 
WHERE Order_Id = 1;

# Get older orders

# 									User stories for products
#

# 									User stories for environment
#


# 									User stories for user information
#


# 									User stories for sessions and authentication
#


####### Worker seller
# 									User stories for orders

# 

# 									User stories for products
# 									User stories for environment
# 									User stories for user information
# 									User stories for sessions and authentication

####### Admin seller
# 									User stories for orders

# 

# 									User stories for products
# 									User stories for environment
# 									User stories for user information
# 									User stories for sessions and authentication
